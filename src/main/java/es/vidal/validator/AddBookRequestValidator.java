package es.vidal.validator;

import es.vidal.modelo.actividad9.CSVPaisDAO;
import es.vidal.modelo.actividad9.PaisDTO;
import es.vidal.modelo.exceptionsActividad9.InvalidParamsException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class AddBookRequestValidator extends RequestValidator {

    private CSVPaisDAO csvPaisDAO;

    private Validator validator;

    public AddBookRequestValidator(HttpServletRequest httpRequest, CSVPaisDAO csvPaisDAO) {
        super(httpRequest);
        this.csvPaisDAO = csvPaisDAO;
        this.validator = new Validator();
    }

    @Override
    public void validate() throws InvalidParamsException , IOException {

        String isbn = getParameter("isbn", "");
        if (!validator.validateISBN(isbn)) {
            addError("isbn", "El isbn debe cumplir este formato ##-##########-#");
        }

        String titulo =  getParameter("titulo", "");
        if (!validator.validateLength(titulo, 20, 120)) {
            addError("titulo", "El titulo debe contener entre 20 y 120 carácteres");
        }

        String numHojas = getParameter("numHojas", "");
        if (!validator.validateRangeNumber(numHojas)) {
            addError("numHojas", "El numero de hojas debe ser mayor que 10");
        }

        String autor =  getParameter("autor", "");
        if (!validator.validateLength(autor, 10, 50)) {
            addError("autor", "El autor debe contener entre 10 y 50 carácteres");
        }

        String fechaCreacion = getParameter("fechaCreacion", "");
        try {
            LocalDateTime.parse(fechaCreacion, DateTimeFormatter.ISO_DATE_TIME);
        } catch (DateTimeParseException e) {
            addError("fechaCreacion", "La fecha de creación debe tener mantener el formato " +
                    "yyyy-MM-dd HH-mm");
        }

        String codPais = getParameter("codPais", "");
        PaisDTO paisDTO = csvPaisDAO.findByCod(codPais);
        if (paisDTO == null){
            addError("codPais", "El codigo de País no es correcto");
        }

        if (hasErrors()) {
            throw new InvalidParamsException(getErrors());
        }

    }

}
