package es.vidal.validator;

import es.vidal.modelo.exceptionsActividad9.InvalidParamsException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;

public abstract class RequestValidator {

    private HashMap<String, String> errors;

    private HttpServletRequest httpRequest;

    public RequestValidator(HttpServletRequest httpRequest) {
        this.httpRequest = httpRequest;
        this.errors = new HashMap<>();
    }

    public abstract void validate() throws InvalidParamsException, InvalidParamsException, IOException;

    protected HashMap<String, String> getErrors() {
        return errors;
    }

    protected void addError(String paramName, String paramMessage) {
        this.errors.put(paramName, paramMessage);
    }

    protected boolean hasErrors() {
        return errors.size() > 0;
    }

    protected String getParameter(String nameParam, String defaultValue) {
        if (httpRequest.getParameter(nameParam) != null) {
            return httpRequest.getParameter(nameParam);
        } else {
            return defaultValue;
        }
    }

    protected String getParameter(String nameParam) {
        return httpRequest.getParameter(nameParam);
    }
}
