package es.vidal.controlador.actividad4;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "TablaMultiplicarServlet", value = "/tabla-multiplicar-numero")
public class TablaMultiplicarNumeroServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        response.setContentType("text/html");
        out.println("<form action='tabla-multiplicar-numero' method='get'>");
        out.println("<label> Nombre <input type=\"text\" name=\"name\"/></label>");
        out.println("<br>");
        out.println("<label> Numero <input type=\"text\" name=\"num\"/></label>");
        out.println("<br>");
        out.println("<input type=\"submit\" value=\"Enviar\">");
        out.println("</form>");

        String numero = request.getParameter("num");
        if ( numero == null || !numero.matches("([1-9]|10)*")){
            out.println("Error introduce un número válido");
        }else{
            int numeroMultiplicar = Integer.parseInt(numero);
            out.println("Tabla del " + numero + "<br>");
            out.println("===========<br>");
            for (int i = 1; i < 11; i++) {
                out.println(numeroMultiplicar + " x " + i + " = " + numeroMultiplicar*i);
                out.println("<br>");
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
