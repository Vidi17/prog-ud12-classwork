package es.vidal.controlador.actividad3;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "PasoParametros", value = "/paso-parametros")
public class PasoParametrosServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            request.getRequestDispatcher("/pasoParametros.jsp").include(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        response.setContentType("text/html");
        out.println("<html><body>");
        out.println("<div><a href=\"pasoParametros.jsp\">Volver</a> </div>");

        String password = request.getParameter("password");
        String repeatPassword = request.getParameter("repeatPassword");

        if (password.equals(repeatPassword)){
            out.println("<h1>OK Procesamiento Formulario POST</h1>");
            out.println("Nombre: " + request.getParameter("name") + "<br>");
            out.println("Email: " + request.getParameter("email") + "<br>");
            out.println("Cumpleaños: " + request.getParameter("birthday") + "<br>");
            out.println("Contraseña: " + password + "<br>");
            out.println("Verificar Contraseña: " + repeatPassword + "<br>");
        }else {
            out.println("<h1>NO OK Procesamiento Formulario POST</h1>");
            out.println("El campo password y repeat password deben ser iguales: ");

        }
        out.println("</body></html>");

    }
}
