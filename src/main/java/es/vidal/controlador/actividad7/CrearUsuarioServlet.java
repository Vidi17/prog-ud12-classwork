package es.vidal.controlador.actividad7;

import es.vidal.modelo.userDAO.FileUserDAO;
import es.vidal.modelo.userDAO.UserDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

@WebServlet(name = "crearUsuario", value = "/crear-usuario")
public class CrearUsuarioServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getRequestDispatcher("/crearUsuario.jsp").include(request, response);
        if (request.getParameter("action").equals("listado-usuarios")){
            response.sendRedirect("listadoUsuarios.jsp");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FileUserDAO fileUserDAO = new FileUserDAO();
        PrintWriter out = response.getWriter();

        response.setContentType("text/html");
        out.println("<html><body>");
        if (request.getParameter("action").equals("Crear")){
            String username = request.getParameter("name");
            String password = request.getParameter("password");
            String email = request.getParameter("email");
            LocalDate birthday = LocalDate.parse(request.getParameter("birthday"));
            UserDTO userDTO = new UserDTO(username,password, email, birthday);
            fileUserDAO.saveUser(userDTO);
            out.println("El usuario ha sido creado con éxito");
            response.sendRedirect("listado-usuarios");
        }else if (request.getParameter("action").equals("Ver detalle")){
            String email = request.getParameter("email");
            UserDTO userDTO = fileUserDAO.findByEmail(email);
            out.println("<html><body>");
            out.println("<h1>Datos usuario " + userDTO.getUsername()+"</h1>");
            out.println("<b>Nombre: </b>" + userDTO.getUsername()+"<br>");
            out.println("<b>Password: </b>" + userDTO.getPassword()+"<br>");
            out.println("<b>Email: </b>" + userDTO.getEmail()+"<br>");
            out.println("<b>Cumpleaños: </b>" + userDTO.getBirthday()+"<br>");
            out.println("<html><body>");
        }
        out.println("<div><a href=\"listadoUsuarios.jsp\">Volver</a> </div>");
        out.println("</body></html>");
    }
}
