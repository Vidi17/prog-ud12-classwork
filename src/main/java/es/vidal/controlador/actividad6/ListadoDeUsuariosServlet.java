package es.vidal.controlador.actividad6;

import es.vidal.modelo.userDAO.FileUserDAO;
import es.vidal.modelo.userDAO.UserDTO;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ListadoDeUsuariosServlet", value = "/listado-usuarios")
public class ListadoDeUsuariosServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        FileUserDAO fileUserDAO = new FileUserDAO();
        ArrayList<UserDTO> usuarios = fileUserDAO.findAll();
        request.setAttribute("usuarios", usuarios);
        request.getRequestDispatcher("listadoUsuarios.jsp").include(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FileUserDAO fileUserDAO = new FileUserDAO();

        response.setContentType("text/html");

        String action = request.getParameter("action");
        if (action.equals("Borrar")) {
            String email = request.getParameter("email");
            fileUserDAO.delete(email);
            response.sendRedirect("listado-usuarios");
        }
    }
}
