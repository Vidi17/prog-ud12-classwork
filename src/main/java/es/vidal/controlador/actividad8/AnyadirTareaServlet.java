package es.vidal.controlador.actividad8;

import es.vidal.modelo.actividad8.SQLTareaDAO;
import es.vidal.modelo.actividad8.TareaDTO;
import es.vidal.modelo.actividad8.TareaDaoInterface;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "AñadirTareaServlet", value = "/añadir-tarea")
public class AnyadirTareaServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        TareaDaoInterface tareaDao = new SQLTareaDAO();
        PrintWriter out = response.getWriter();
        TareaDTO tareaDTO = tareaDao.getTareaFromRequest(request);
        response.setContentType("text/html");

        String action = request.getParameter("action");
        if (action.equals("Añadir")){
            boolean inserted = tareaDao.insert(tareaDTO);
            if (!inserted){
                out.println("La tarea ha sido añadida con éxito");
            }else {
                tareaDao.saveTarea(tareaDTO);
                out.println("La tarea no ha sido añadida con éxito");
            }
            volverAPaginaPrincipal(out);
        }else {
            response.sendRedirect("mostrar-tareas");
        }
    }

    private void volverAPaginaPrincipal(PrintWriter out){
        out.println("<html><body>");
        out.println("<div><a href=\"mostrar-tareas\">Volver</a></div>");
        out.println("</body></html>");
    }
}
