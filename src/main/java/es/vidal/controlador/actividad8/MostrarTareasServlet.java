package es.vidal.controlador.actividad8;

import es.vidal.modelo.actividad8.SQLTareaDAO;
import es.vidal.modelo.actividad8.TareaDTO;
import es.vidal.modelo.actividad8.TareaDaoInterface;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "MostrarTareasServlet", value = "/mostrar-tareas")
public class MostrarTareasServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        TareaDaoInterface tareaDaoInterface = new SQLTareaDAO();
        ArrayList<TareaDTO> tareaDTOS = tareaDaoInterface.findAll();
        request.setAttribute("tareas", tareaDTOS);
        request.getRequestDispatcher("/actividad8/listadoTareas.jsp").include(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
