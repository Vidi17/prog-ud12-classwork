package es.vidal.controlador.actividad8;

import es.vidal.modelo.actividad8.SQLTareaDAO;
import es.vidal.modelo.actividad8.TareaDTO;
import es.vidal.modelo.actividad8.TareaDaoInterface;
import es.vidal.modelo.actividad8.TareaNotFoundException;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "MarcarTareaServlet", value = "/marcar-tarea")
public class MarcarTareaServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        TareaDaoInterface tareaDao = new SQLTareaDAO();
        String action = request.getParameter("action");
        int tareaId = Integer.parseInt(request.getParameter("id"));

        if (action.equals("Realizada")){
            try {
                TareaDTO tareaDTO = tareaDao.findById(tareaId);
                tareaDTO.setRealizada(true);
                tareaDao.saveTarea(tareaDTO);
            }catch (TareaNotFoundException exception){
                exception.getStackTrace();
            }
        }else if (action.equals("No realizada")){
            try {
                TareaDTO tareaDTO = tareaDao.findById(tareaId);
                tareaDTO.setRealizada(false);
                tareaDao.saveTarea(tareaDTO);
            }catch (TareaNotFoundException exception){
                exception.getStackTrace();
            }
        }
        response.sendRedirect("mostrar-tareas");
    }
}
