package es.vidal.controlador.actividad8;

import es.vidal.modelo.actividad8.SQLTareaDAO;
import es.vidal.modelo.actividad8.TareaDaoInterface;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "EditarTareaServlet", value = "/modificar-tarea")
public class EditarTareaServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getRequestDispatcher("/actividad8/modificarTarea.jsp").include(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        TareaDaoInterface tareaDao = new SQLTareaDAO();
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        String action = request.getParameter("action");
        if (action.equals("Modificar")){
            boolean edited = tareaDao.editarTareaFromRequest(request);
            if (!edited){
                out.println("La tarea ha sido editada con éxito");
            }else {
                out.println("La tarea no ha sido editada con éxito");
            }
            volverAPaginaPrincipal(out);
        }else {
            response.sendRedirect("mostrar-tareas");
        }
    }

    private void volverAPaginaPrincipal(PrintWriter out){
        out.println("<html><body>");
        out.println("<div><a href=\"mostrar-tareas\">Volver</a></div>");
        out.println("</body></html>");
    }
}
