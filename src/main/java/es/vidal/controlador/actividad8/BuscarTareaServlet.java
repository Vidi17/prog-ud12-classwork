package es.vidal.controlador.actividad8;

import es.vidal.modelo.actividad8.SQLTareaDAO;
import es.vidal.modelo.actividad8.TareaDTO;
import es.vidal.modelo.actividad8.TareaDaoInterface;
import es.vidal.modelo.actividad8.TareaNotFoundException;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "BuscarTareaServlet", value = "/buscar-tarea")
public class BuscarTareaServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        TareaDaoInterface tareaDao = new SQLTareaDAO();
        String realizada = request.getParameter("realizada");
        if (realizada == null){
            realizada = "";
        }
        String action = request.getParameter("action");
        String descripcion = request.getParameter("descripcion");
        ArrayList<TareaDTO> tareas = new ArrayList<>();
        ArrayList<TareaDTO> tareasRealizadas = new ArrayList<>();
        ArrayList<TareaDTO> tareasBuscadas = new ArrayList<>();

        try {
            tareas = tareaDao.findByDescripcion(descripcion);
            tareasRealizadas = tareaDao.findByRealizada();
        }catch (TareaNotFoundException exception){
            exception.getStackTrace();
        }

        if (action.equals("Buscar") && realizada.equals("isRealizada")){
            for (TareaDTO tarea : tareas) {
                for (TareaDTO tareasRealizada : tareasRealizadas) {
                    if (tarea.equals(tareasRealizada)) {
                        tareasBuscadas.add(tarea);
                    }
                }
            }
        }else if (action.equals("Buscar")){
            for (TareaDTO tarea: tareas) {
                if (!tarea.isRealizada()){
                    tareasBuscadas.add(tarea);
                }
            }
        }
        request.setAttribute("tareas", tareasBuscadas);
        request.getRequestDispatcher("/actividad8/listadoTareas.jsp").include(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
