package es.vidal.controlador;

import java.io.*;
import java.util.Random;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "bolaBingo", value = "/bola-bingo")
public class BingoServlet extends HttpServlet {
    int numeroBola;

    public void init() {
        Random numeroBolaRandom = new Random();
        numeroBola = numeroBolaRandom.nextInt(101);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" +"Bola: " + numeroBola + "</h1>");
        out.println("</body></html>");
    }

    public void destroy() {
    }
}