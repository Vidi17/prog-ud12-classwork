package es.vidal.controlador.actividad1;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

@WebServlet(name = "verHora", value = "/ver-hora")
public class HoraActualServlet extends HttpServlet {

    public void init() {
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalTime madrid = LocalTime.now(ZoneId.of("Europe/Madrid"));
        LocalTime londres = LocalTime.now(ZoneId.of("Europe/London"));
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1> Hora Madrid: " + madrid.format(formato) + "</h1>");
        out.println("<h1> Hora Londres: " + londres.format(formato) + "</h1>");
        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
