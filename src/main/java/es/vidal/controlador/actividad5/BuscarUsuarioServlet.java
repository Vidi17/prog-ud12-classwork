package es.vidal.controlador.actividad5;

import es.vidal.modelo.userDAO.FileUserDAO;
import es.vidal.modelo.userDAO.UserDTO;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "BuscarUsuarios", value = "/buscar-usuarios")
public class BuscarUsuarioServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(request.getParameter("email") == null){
            request.getRequestDispatcher("/buscarUsuario.jsp").include(request, response);
        }else{
            metodo(request, response);
        }

    }
    public void metodo(HttpServletRequest request, HttpServletResponse response) throws IOException {

        FileUserDAO fileUserDAO = new FileUserDAO();
        PrintWriter out = response.getWriter();
        String email = request.getParameter("email");
        UserDTO userDTO = fileUserDAO.findByEmail(email);
        if (userDTO != null){
            out.println("<html><body>");
            out.println("<h1>Datos usuario "+userDTO.getUsername()+"</h1>");
            out.println("<b>Nombre: </b>"+userDTO.getUsername()+"<br>");
            out.println("<b>Password: </b>"+userDTO.getPassword()+"<br>");
            out.println("<b>Email: </b>"+userDTO.getEmail()+"<br>");
            out.println("<b>Cumpleaños: </b>"+userDTO.getBirthday()+"<br>");
            out.println("<html><body>");
        }else {
            out.println("<html><body>");
            out.println("<h1>ERROR</h1>");
            out.println("El usuario no ha sido encontrado :'(");
            out.println("<html><body>");
        }
        out.println("<div><a href=\"buscarUsuario.jsp\">Volver</a> </div>");
    }
}
