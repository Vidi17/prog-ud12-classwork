package es.vidal.controlador.actividad2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "tablaMultiplicar", value = "/tabla-multiplicar")
public class TablasMultiplicarServlet extends HttpServlet {

    public void init() {
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        for (int i = 1; i < 11; i++) {
            out.println("<h1> Tabla del " + i + "</h1>");
            out.println("<h1> ======== </h1>");
            for (int j = 1; j < 11; j++) {
                out.println("<h2>" + i + " x " + j + " = " + i * j + " </h2>");
            }
            out.println("<br>");
        }
        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
