package es.vidal.controlador.actividad9;

import es.vidal.modelo.actividad9.CSVPaisDAO;
import es.vidal.modelo.actividad9.LibroDTO;
import es.vidal.modelo.actividad9.PaisDTO;
import es.vidal.modelo.actividad9.SQLlibroDAO;
import es.vidal.modelo.exceptionsActividad9.InvalidParamsException;
import es.vidal.validator.AddBookRequestValidator;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "AnyadirLibroServlet", value = "/anyadir-libros")
public class AnyadirLibroServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CSVPaisDAO csvPaisDAO = new CSVPaisDAO();
        ArrayList<PaisDTO> paisDTOArrayList = csvPaisDAO.findAll();
        request.getSession().setAttribute("paises", paisDTOArrayList);
        request.getRequestDispatcher("/actividad9/actividad9.jsp").include(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CSVPaisDAO csvPaisDAO = new CSVPaisDAO();
        SQLlibroDAO sqLlibroDAO = new SQLlibroDAO();
        AddBookRequestValidator bookRequestValidator = new AddBookRequestValidator(request , csvPaisDAO);
        request.getSession().removeAttribute("errors");
        String parameterAction = request.getParameter("action");

        if (parameterAction.equals("Crear")){
            try {
                bookRequestValidator.validate();
                LibroDTO libroDTO = sqLlibroDAO.getLibroFromRequest(request);
                sqLlibroDAO.insert(libroDTO);
            }catch (InvalidParamsException exception){
                request.getSession().setAttribute("errors", exception.getErrors());
                request.getRequestDispatcher("/actividad9/actividad9.jsp").include(request, response);
            }finally {
                response.sendRedirect("mostrar-libros");
            }
        }else {
            request.getRequestDispatcher("mostrar-libros").include(request, response);
        }
    }
}
