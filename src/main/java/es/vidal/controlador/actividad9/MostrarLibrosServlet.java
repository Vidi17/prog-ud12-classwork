package es.vidal.controlador.actividad9;

import es.vidal.modelo.actividad8.SQLTareaDAO;
import es.vidal.modelo.actividad8.TareaDTO;
import es.vidal.modelo.actividad8.TareaDaoInterface;
import es.vidal.modelo.actividad9.LibroDTO;
import es.vidal.modelo.actividad9.LibroDaoInterface;
import es.vidal.modelo.actividad9.SQLlibroDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "MostrarLibrosServlet", value = "/mostrar-libros")
public class MostrarLibrosServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LibroDaoInterface libroDaoInterface = new SQLlibroDAO();
        ArrayList<LibroDTO> libroDTOS = libroDaoInterface.findAll();
        request.setAttribute("libros", libroDTOS);
        request.getRequestDispatcher("/actividad9/listarLibros.jsp").include(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
