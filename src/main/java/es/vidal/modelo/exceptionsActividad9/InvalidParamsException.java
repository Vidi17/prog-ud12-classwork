package es.vidal.modelo.exceptionsActividad9;

import java.util.HashMap;

public class InvalidParamsException extends Exception {

    HashMap<String, String> errors;

    public InvalidParamsException(HashMap<String, String> errors) {
        super("Parámetros inválidos");
        this.errors = errors;
    }

    public HashMap<String, String> getErrors() {
        return errors;
    }

}
