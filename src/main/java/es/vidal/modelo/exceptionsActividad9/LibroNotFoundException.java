package es.vidal.modelo.exceptionsActividad9;

public class LibroNotFoundException extends Exception {

    public LibroNotFoundException(String email) {

        super("El usuario " + email + "no se encuentra en la bd");

    }

}
