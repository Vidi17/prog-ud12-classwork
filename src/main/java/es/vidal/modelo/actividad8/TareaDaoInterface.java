package es.vidal.modelo.actividad8;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public interface TareaDaoInterface {

    /**
     * Devuelve todos los usuarios presentes en el fichero
     *
     * @return ArrayList<TareaDTO>
     */
    ArrayList<TareaDTO> findAll();

    /**
     * Devuelve un usuario cuyo email correspode con @email
     *
     * @return TareaDTO
     */
    ArrayList<TareaDTO> findByDescripcion(String descripcion) throws TareaNotFoundException;

    /**
     * Devuelve un usuario cuyo email correspode con @email si no lo encuentra
     * lanza una excepción @UserNotFoundException
     *
     * @return TareaDTO
     */
    ArrayList<TareaDTO> findByRealizada() throws TareaNotFoundException;

    /**
     * Devuelve un usuario cuyo username correspode con @username
     *
     * @return TareaDTO
     */
    TareaDTO findById(int id) throws TareaNotFoundException;


    /**
     * Inserta un nuevo usuario en la bd
     *
     * @return boolean indicando si la operacion se ha realizado correctamente
     */
    boolean delete(int id);

    boolean insert(TareaDTO tareaDTO);

    void saveTarea(TareaDTO tareaDTO);

    boolean update(TareaDTO tareaDTO);

    TareaDTO getTareaFromRequest(HttpServletRequest request);

    boolean editarTareaFromRequest(HttpServletRequest request);

    Prioridad prioridadToEnum(String prioridad);
}
