package es.vidal.modelo.actividad8;

import java.time.LocalDateTime;
import java.util.Objects;

public class TareaDTO {

    private int id;

    private Prioridad prioridad;

    private String descripcion;

    private LocalDateTime fechaCreacion;

    private LocalDateTime fechaEntrega;

    private boolean realizada;

    public TareaDTO(String descripcion, LocalDateTime fechaEntrega) {
        id = -1;
        prioridad = Prioridad.MEDIA;
        this.descripcion = descripcion;
        fechaCreacion = LocalDateTime.now();
        this.fechaEntrega = fechaEntrega;
        realizada = false;
    }

    public TareaDTO(String descripcion, Prioridad prioridad, LocalDateTime fechaEntrega) {
        id = -1;
        this.prioridad = prioridad;
        this.descripcion = descripcion;
        fechaCreacion = LocalDateTime.now();
        this.fechaEntrega = fechaEntrega;
        realizada = false;
    }

    public TareaDTO(int id, Prioridad prioridad, String descripcion, LocalDateTime fechaEntrega) {
        this.id = id;
        this.prioridad = prioridad;
        this.descripcion = descripcion;
        fechaCreacion = LocalDateTime.now();
        this.fechaEntrega = fechaEntrega;
        realizada = false;
    }

    public TareaDTO(int id, Prioridad prioridad, String descripcion, LocalDateTime fechaCreacion
            , LocalDateTime fechaEntrega, boolean realizada) {
        this.id = id;
        this.prioridad = prioridad;
        this.descripcion = descripcion;
        this.fechaCreacion = fechaCreacion;
        this.fechaEntrega = fechaEntrega;
        this.realizada = realizada;
    }

    public void setRealizada(boolean realizada) {
        this.realizada = realizada;
    }

    public int getId() {
        return id;
    }

    public Prioridad getPrioridad() {
        return prioridad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public LocalDateTime getFechaCreacion() {
        return fechaCreacion;
    }

    public LocalDateTime getFechaEntrega() {
        return fechaEntrega;
    }

    public boolean isRealizada() {
        return realizada;
    }

    @Override
    public String toString() {
        return "Tarea " +
                "Id = " + id +
                ", Prioridad = '" + prioridad.toString() + '\'' +
                ", Descripción = '" + descripcion + '\'' +
                ", Fecha de creación = " + fechaCreacion +
                ", Fecha de entrega = " + fechaEntrega +
                ", Realizada = " + realizada;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TareaDTO tareaDTO = (TareaDTO) o;
        return id == tareaDTO.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
