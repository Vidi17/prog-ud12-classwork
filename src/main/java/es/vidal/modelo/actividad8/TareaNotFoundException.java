package es.vidal.modelo.actividad8;

public class TareaNotFoundException extends Exception{
    public TareaNotFoundException(String message) {
        super(message);
    }
}
