package es.vidal.modelo.actividad8;

import es.vidal.MySQLConnection;

import javax.servlet.http.HttpServletRequest;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class SQLTareaDAO implements TareaDaoInterface {

    private MySQLConnection mySQLConnection;

    private Connection connection;

    public SQLTareaDAO(){
        mySQLConnection = new MySQLConnection("192.168.0.106", "TODOLIST","batoi", "1234");
        connection = mySQLConnection.getConnection();
    }

    @Override
    public ArrayList<TareaDTO> findAll() {
        String sql = "SELECT * FROM Tareas";
        return getTareaDTOS( sql);
    }

    @Override
    public ArrayList<TareaDTO> findByDescripcion(String descripcion) throws TareaNotFoundException{
        ArrayList<TareaDTO> tareasList = new ArrayList<>();
        String sql = "SELECT * FROM Tareas WHERE descripcion LIKE ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            preparedStatement.setString(1, "%" + descripcion + "%");
            ResultSet tareas = preparedStatement.executeQuery();
            while (tareas.next()){
                TareaDTO tareaDTO = getTareaFromResulset(tareas);
                tareasList.add(tareaDTO);
            }
        }catch (SQLException exception){
            exception.getStackTrace();
        }
        if (tareasList.isEmpty()){
            throw new TareaNotFoundException("No hay ninguna tarea realizada");
        }else {
            return tareasList;
        }
    }

    @Override
    public ArrayList<TareaDTO> findByRealizada() throws TareaNotFoundException{
        String sql = "SELECT * FROM Tareas WHERE realizada = TRUE";
        ArrayList<TareaDTO> tareaDTOS = getTareaDTOS( sql);
        if (tareaDTOS.isEmpty()){
            throw new TareaNotFoundException("No hay ninguna tarea realizada");
        }else {
            return tareaDTOS;
        }
    }

    @Override
    public TareaDTO findById(int id) throws TareaNotFoundException{
        String sql = String.format("SELECT * FROM Tareas WHERE id = %d", id);
        ArrayList<TareaDTO> tareaDTOS = getTareaDTOS( sql);
        if (tareaDTOS.isEmpty()){
            throw new TareaNotFoundException("Tarea no encontrada");
        }else {
            return tareaDTOS.get(0);
        }
    }

    private ArrayList<TareaDTO> getTareaDTOS(String sql) {
        ArrayList<TareaDTO> tareasList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            ResultSet tareas = preparedStatement.executeQuery();
            while (tareas.next()){
                TareaDTO tareaDTO = getTareaFromResulset(tareas);
                tareasList.add(tareaDTO);
            }
        }catch (SQLException exception){
            exception.getStackTrace();
        }
        return tareasList;
    }

    @Override
    public void saveTarea(TareaDTO tareaDTO){
        ArrayList<TareaDTO> tareaDTOArrayList = findAll();
        if (tareaDTOArrayList.contains(tareaDTO)){
            update(tareaDTO);
        }else {
            insert(tareaDTO);
        }
    }

    @Override
    public boolean insert(TareaDTO tareaDTO) {
        boolean executed = false;
        String sql = "INSERT INTO Tareas (prioridad, descripcion, fechaCreacion, fechaEntrega, realizada)" +
                " VALUES (?,?,?,?,?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            preparedStatement.setString(1, tareaDTO.getPrioridad().toString());
            preparedStatement.setString(2, tareaDTO.getDescripcion());
            LocalDateTime fechaCreacion = tareaDTO.getFechaCreacion();
            preparedStatement.setTimestamp(3, Timestamp.valueOf(fechaCreacion));
            LocalDateTime fechaEntrega = tareaDTO.getFechaEntrega();
            preparedStatement.setTimestamp(4, Timestamp.valueOf(fechaEntrega));
            preparedStatement.setBoolean(5, tareaDTO.isRealizada());
            executed = preparedStatement.execute();
        }catch (SQLException exception){
            exception.getStackTrace();
        }
        return executed;
    }

    @Override
    public boolean delete(int id) {
        boolean executed = false;
        String sql = String.format("DELETE FROM Tareas WHERE id = %d", id);
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            executed = preparedStatement.execute();
        }catch (SQLException exception){
            exception.getStackTrace();
        }
        return executed;
    }

    @Override
    public boolean update(TareaDTO tareaDTO) {
        boolean deleted = delete(tareaDTO.getId());
        boolean inserted = false;
        if (!deleted) {
            inserted = insert(tareaDTO);
        }
        return inserted;
    }

    @Override
    public TareaDTO getTareaFromRequest(HttpServletRequest request){
        String descripcionTarea = request.getParameter("descripcion");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String prioridad = request.getParameter("prioridad");
        Prioridad prioridadEnum = prioridadToEnum(prioridad);
        String fechaEntregaString = request.getParameter("fechaEntrega");
        fechaEntregaString = fechaEntregaString + " 23:59:00";
        LocalDateTime fechaEntregaTarea = LocalDateTime.parse(fechaEntregaString, dateTimeFormatter);
        return new TareaDTO(descripcionTarea, prioridadEnum, fechaEntregaTarea);
    }

    @Override
    public boolean editarTareaFromRequest(HttpServletRequest request){
        int id = Integer.parseInt(request.getParameter("id"));
        String prioridadString = request.getParameter("prioridad");
        Prioridad prioridad = prioridadToEnum(prioridadString);
        String descripcionTarea = request.getParameter("descripcion");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime fechaCreacionTarea = LocalDateTime.parse(request.getParameter("fechaCreacion"));
        String fechaEntregaString = request.getParameter("fechaEntrega") + " 23:59:00";
        LocalDateTime fechaEntregaTarea = LocalDateTime.parse(fechaEntregaString, dateTimeFormatter);
        String isRealizada = request.getParameter("realizada");
        if (isRealizada == null){
            isRealizada = "";
        }
        boolean realizada = (isRealizada.equals("isRealizada"));
        TareaDTO tareaDTO = new TareaDTO(id, prioridad, descripcionTarea, fechaCreacionTarea, fechaEntregaTarea
                , realizada);

       return update(tareaDTO);
    }

    @Override
    public Prioridad prioridadToEnum(String prioridad){
        return switch (prioridad){
            case "ALTA" -> Prioridad.ALTA;
            case "MEDIA" -> Prioridad.MEDIA;
            case  "BAJA" -> Prioridad.BAJA;
            default -> null;
        };
    }

    private TareaDTO getTareaFromResulset(ResultSet resultSet) throws SQLException {
        int id = Integer.parseInt(resultSet.getString("id"));
        String prioridadString = resultSet.getString("prioridad");
        Prioridad prioridadTarea = prioridadToEnum(prioridadString);
        String descripcionTarea = resultSet.getString("descripcion");
        LocalDateTime fechaCreacionTarea = resultSet.getTimestamp("fechaCreacion").toLocalDateTime();
        LocalDateTime fechaEntregaTarea = resultSet.getTimestamp("fechaEntrega").toLocalDateTime();
        boolean realizada = resultSet.getBoolean("realizada");
        return new TareaDTO(id, prioridadTarea, descripcionTarea, fechaCreacionTarea, fechaEntregaTarea, realizada);
    }
}
