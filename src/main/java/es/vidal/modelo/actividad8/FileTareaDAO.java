package es.vidal.modelo.actividad8;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class FileTareaDAO implements TareaDaoInterface {

    private final File FILE;

    private static final AtomicInteger AUTO_INCREMENT_ID = new AtomicInteger(0);

    public FileTareaDAO(){
        ClassLoader classLoader = getClass().getClassLoader();
        FILE = new File(classLoader.getResource("buscarTarea/fichero.txt").getFile());
    }

    @Override
    public ArrayList<TareaDTO> findAll() {
        ArrayList<TareaDTO> tareasList = new ArrayList<>();
        String line;
        try (FileReader fileReader = new FileReader(FILE);
             BufferedReader bufferedReader = new BufferedReader(fileReader)){
            do {
                line = bufferedReader.readLine();
                if (line != null){
                    TareaDTO tareaToAdd = createTareaDTO(line);
                    tareasList.add(tareaToAdd);
                }
            }while (line != null);
        }catch (IOException exception){
            exception.getStackTrace();
        }
        return tareasList;
    }

    public TareaDTO createTareaDTO(String line){
        String[] tareaDTO = line.split(";");
        int id = Integer.parseInt(tareaDTO[0]);
        Prioridad prioridad = prioridadToEnum(tareaDTO[1]);
        String descripcion =  tareaDTO[2];
        LocalDateTime fechaCreacion = LocalDateTime.parse(tareaDTO[3]);
        LocalDateTime fechaEntrega = LocalDateTime.parse(tareaDTO[4]);
        boolean realizada = realizadaToBoolean(tareaDTO[5]);
        return new TareaDTO(id, prioridad, descripcion, fechaCreacion, fechaEntrega, realizada);
    }

    private boolean realizadaToBoolean(String realizada){
        return (realizada.equals("true"));
    }

    private String realizadaToString(boolean realizada){
        if (realizada){
            return "true";
        }else {
            return "false";
        }
    }

    public String tareaToString(TareaDTO tareaDTO){
        return String.format("%d;%s;%s;%s;%s;%s",tareaDTO.getId(), tareaDTO.getPrioridad(), tareaDTO.getDescripcion()
                , tareaDTO.getFechaCreacion().toString(), tareaDTO.getFechaEntrega().toString()
                , realizadaToString(tareaDTO.isRealizada())
        );
    }

    @Override
    public ArrayList<TareaDTO> findByDescripcion(String descripcion) {
        ArrayList<TareaDTO> tareaDTOArrayList = findAll();
        ArrayList<TareaDTO> tareasBuscadas = new ArrayList<>();

        for (TareaDTO tarea: tareaDTOArrayList) {
            if (tarea.getDescripcion().contains(descripcion)){
                tareasBuscadas.add(tarea);
            }
        }
        return tareasBuscadas;
    }

    @Override
    public ArrayList<TareaDTO> findByRealizada() throws TareaNotFoundException {
        ArrayList<TareaDTO> tareaDTOArrayList = findAll();
        ArrayList<TareaDTO> tareasBuscadas = new ArrayList<>();

        for (TareaDTO tarea: tareaDTOArrayList) {
            if (tarea.isRealizada()){
                tareasBuscadas.add(tarea);
            }
        }
        if (tareasBuscadas.isEmpty()){
            throw new TareaNotFoundException("No hay ninguna tarea realizada");
        }else {
            return tareasBuscadas;
        }
    }

    @Override
    public TareaDTO findById(int id) throws TareaNotFoundException {
        ArrayList<TareaDTO> tareaDTOArrayList = findAll();

        for (TareaDTO tarea: tareaDTOArrayList) {
            if (tarea.getId() == id){
                return tarea;
            }
        }
        throw new TareaNotFoundException("La tarea buscada no ha sido encontrada");
    }

    @Override
    public TareaDTO getTareaFromRequest(HttpServletRequest request) {
        int id = AUTO_INCREMENT_ID.getAndIncrement();
        String descripcionTarea = request.getParameter("descripcion");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String fechaEntregaString = request.getParameter("fechaEntrega");
        String prioridad = request.getParameter("prioridad");
        Prioridad prioridadEnum = prioridadToEnum(prioridad);
        fechaEntregaString = fechaEntregaString + " 23:59:00";
        LocalDateTime fechaEntregaTarea = LocalDateTime.parse(fechaEntregaString, dateTimeFormatter);
        return new TareaDTO(id, prioridadEnum, descripcionTarea, fechaEntregaTarea);
    }

    @Override
    public boolean editarTareaFromRequest(HttpServletRequest request) {
        int id = Integer.parseInt(request.getParameter("id"));
        String prioridadString = request.getParameter("prioridad");
        Prioridad prioridad = prioridadToEnum(prioridadString);
        String descripcionTarea = request.getParameter("descripcion");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime fechaCreacionTarea = LocalDateTime.parse(request.getParameter("fechaCreacion"));
        String fechaEntregaString = request.getParameter("fechaEntrega") + " 23:59:00";
        LocalDateTime fechaEntregaTarea = LocalDateTime.parse(fechaEntregaString, dateTimeFormatter);
        String isRealizada = request.getParameter("realizada");
        if (isRealizada == null){
            isRealizada = "";
        }
        boolean realizada = (isRealizada.equals("isRealizada"));
        TareaDTO tareaDTO = new TareaDTO(id, prioridad, descripcionTarea, fechaCreacionTarea, fechaEntregaTarea
                , realizada);

        return update(tareaDTO);
    }

    @Override
    public Prioridad prioridadToEnum(String prioridad) {
        return switch (prioridad){
            case "ALTA" -> Prioridad.ALTA;
            case "MEDIA" -> Prioridad.MEDIA;
            case  "BAJA" -> Prioridad.BAJA;
            default -> null;
        };
    }

    @Override
    public void saveTarea (TareaDTO tareaDTO){
        ArrayList<TareaDTO> tareasList = findAll();
        if (tareasList.contains(tareaDTO)){
            update(tareaDTO);
        }else {
            insert(tareaDTO);
        }
    }

    @Override
    public boolean insert(TareaDTO tareaDTO) {
        try (FileWriter fileWriter = new FileWriter(FILE, true);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)){
            String tareaToAdd = tareaToString(tareaDTO);
            bufferedWriter.append(tareaToAdd);
            bufferedWriter.newLine();
        }catch (IOException ex){
            ex.getStackTrace();
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(int id) {
        ArrayList<String> tareasList = new ArrayList<>();
        String tareaString;
        boolean deleted = true;

        try (FileReader fileReader = new FileReader(FILE);
             BufferedReader bufferedReader = new BufferedReader(fileReader)){
            do {
                tareaString = bufferedReader.readLine();
                if (tareaString != null){
                    String[] tareaToDelete = tareaString.split(";");
                    int tareaId = Integer.parseInt(tareaToDelete[0]);

                    if (tareaId != id){
                        tareasList.add(tareaString);
                    }else {
                        deleted = false;
                    }
                }
            }while (tareaString != null);
        }catch (IOException ex){
            ex.getStackTrace();
        }

        try (FileWriter fileWriter = new FileWriter(FILE);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)){
            if (!tareasList.isEmpty()){
                String tarea = tareasList.get(0);
                bufferedWriter.write(tarea);
                bufferedWriter.newLine();

                for (int i = 1; i < tareasList.size(); i++) {
                    bufferedWriter.append(tareasList.get(i));
                    bufferedWriter.newLine();
                }
            }
        }catch (IOException ex){
            ex.getStackTrace();
            return true;
        }
        return deleted;
    }

    @Override
    public boolean update(TareaDTO tareaDTO) {
        boolean deleted = delete(tareaDTO.getId());
        boolean inserted = true;
        if (!deleted) {
            inserted = insert(tareaDTO);
        }
        return inserted;
    }
}
