package es.vidal.modelo.actividad9;

import es.vidal.modelo.actividad8.Prioridad;
import es.vidal.modelo.actividad8.TareaDTO;
import es.vidal.modelo.actividad8.TareaNotFoundException;
import es.vidal.modelo.exceptionsActividad9.LibroNotFoundException;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public interface LibroDaoInterface {

    /**
     * Devuelve todos los usuarios presentes en el fichero
     *
     * @return ArrayList<TareaDTO>
     */
    ArrayList<LibroDTO> findAll();

    /**
     * Devuelve un usuario cuyo email correspode con @email
     *
     * @return TareaDTO
     */
    ArrayList<LibroDTO> findByIsbn(String isbn) throws LibroNotFoundException;

    /**
     * Devuelve un usuario cuyo email correspode con @email si no lo encuentra
     * lanza una excepción @UserNotFoundException
     *
     * @return TareaDTO
     */

    boolean delete(String isbn);

    boolean insert(LibroDTO libroDTO);

    void saveLibro(LibroDTO libroDTO);

    boolean update(LibroDTO libroDTO);

    LibroDTO getLibroFromRequest(HttpServletRequest request);

}
