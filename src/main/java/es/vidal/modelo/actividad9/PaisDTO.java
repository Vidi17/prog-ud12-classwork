package es.vidal.modelo.actividad9;

public class PaisDTO {

    private String nombre;

    private String name;

    private String nom;

    private String iso2;

    private String iso3;

    private int phone_code;

    public PaisDTO(String nombre, String name, String nom, String iso2, String iso3, int phone_code) {
        this.nombre = nombre;
        this.name = name;
        this.nom = nom;
        this.iso2 = iso2;
        this.iso3 = iso3;
        this.phone_code = phone_code;
    }

    public String getNombre() {
        return nombre;
    }

    public String getName() {
        return name;
    }

    public String getNom() {
        return nom;
    }

    public String getIso2() {
        return iso2;
    }

    public String getIso3() {
        return iso3;
    }

    public int getPhone_code() {
        return phone_code;
    }
}
