package es.vidal.modelo.actividad9;

import java.time.LocalDateTime;
import java.util.Objects;

public class LibroDTO {

    private String isbn;

    private String titulo;

    private int numHojas;

    private String autor;

    private LocalDateTime fechaCreacion;

    private PaisDTO codPais;

    public LibroDTO(String isbn, String titulo, int numHojas, String autor, LocalDateTime fechaCreacion, PaisDTO codPais) {
        this.isbn = isbn;
        this.titulo = titulo;
        this.numHojas = numHojas;
        this.autor = autor;
        this.fechaCreacion = fechaCreacion;
        this.codPais = codPais;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getTitulo() {
        return titulo;
    }

    public int getNumHojas() {
        return numHojas;
    }

    public String getAutor() {
        return autor;
    }

    public LocalDateTime getFechaCreacion() {
        return fechaCreacion;
    }

    public PaisDTO getCodPais() {
        return codPais;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LibroDTO libroDTO = (LibroDTO) o;
        return Objects.equals(isbn, libroDTO.isbn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isbn);
    }
}

