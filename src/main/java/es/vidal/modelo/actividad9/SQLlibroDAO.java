package es.vidal.modelo.actividad9;
import es.vidal.MySQLConnection;

import javax.servlet.http.HttpServletRequest;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class SQLlibroDAO implements LibroDaoInterface{

    private MySQLConnection mySQLConnection;

    private Connection connection;

    private CSVPaisDAO csvPaisDAO = new CSVPaisDAO();;

    public SQLlibroDAO(){
        mySQLConnection = new MySQLConnection("192.168.0.112", "gestor_libros_db","batoi", "1234");
        connection = mySQLConnection.getConnection();
    }

    public ArrayList<LibroDTO> findAll() {
        String sql = "SELECT * FROM LIBROS";
        return getLibroDTOS(sql);
    }

    @Override
    public ArrayList<LibroDTO> findByIsbn(String isbn) {
        ArrayList<LibroDTO> librosList = new ArrayList<>();
        String sql = "SELECT * FROM LIBROS WHERE isbn LIKE ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            preparedStatement.setString(1, "%" + isbn + "%");
            ResultSet tareas = preparedStatement.executeQuery();
            while (tareas.next()){
                LibroDTO libroDTO = getLibroFromResulset(tareas);
                librosList.add(libroDTO);
            }
        }catch (SQLException exception){
            exception.getStackTrace();
        }
        return librosList;
    }

    private ArrayList<LibroDTO> getLibroDTOS(String sql) {
        ArrayList<LibroDTO> librosList = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            ResultSet libros = preparedStatement.executeQuery();
            while (libros.next()){
                LibroDTO libroDTO = getLibroFromResulset(libros);
                librosList.add(libroDTO);
            }
        }catch (SQLException exception){
            exception.getStackTrace();
        }
        return librosList;
    }

    @Override
    public void saveLibro(LibroDTO libroDTO){
        ArrayList<LibroDTO> tareaDTOArrayList = findAll();
        if (tareaDTOArrayList.contains(libroDTO)){
            update(libroDTO);
        }else {
            insert(libroDTO);
        }
    }

    @Override
    public boolean insert(LibroDTO libroDTO) {
        boolean executed = false;
        String sql = "INSERT INTO LIBROS (isbn, titulo, num_hojas, autor, fecha_creacion, cod_pais)" +
                " VALUES (?,?,?,?,?,?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            preparedStatement.setString(1, libroDTO.getIsbn());
            preparedStatement.setString(2, libroDTO.getTitulo());
            preparedStatement.setInt(3, libroDTO.getNumHojas());
            preparedStatement.setString(4, libroDTO.getAutor());
            LocalDateTime fechaCreacion = libroDTO.getFechaCreacion();
            preparedStatement.setTimestamp(5, Timestamp.valueOf(fechaCreacion));
            preparedStatement.setString(6, libroDTO.getCodPais().getIso2());
            executed = preparedStatement.execute();
        }catch (SQLException exception){
            exception.getStackTrace();
        }
        return executed;
    }

    @Override
    public boolean delete(String isbn) {
        boolean executed = false;
        String sql = String.format("DELETE FROM LIBROS WHERE isbn = %s", isbn);
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            executed = preparedStatement.execute();
        }catch (SQLException exception){
            exception.getStackTrace();
        }
        return executed;
    }

    @Override
    public boolean update(LibroDTO libroDTO) {
        boolean deleted = delete(libroDTO.getIsbn());
        boolean inserted = false;
        if (!deleted) {
            inserted = insert(libroDTO);
        }
        return inserted;
    }

    @Override
    public LibroDTO getLibroFromRequest(HttpServletRequest request){
        String isbn = request.getParameter("isbn");
        String titulo = request.getParameter("titulo");
        int numHojas = Integer.parseInt(request.getParameter("numHojas"));
        String autor = request.getParameter("autor");
        String codPais = request.getParameter("codPais");
        PaisDTO paisDTO = csvPaisDAO.findByCod(codPais);
        String fechaCreacionString = request.getParameter("fechaCreacion");
        LocalDateTime fechaCreacion = LocalDateTime.parse(fechaCreacionString, DateTimeFormatter.ISO_DATE_TIME);
        return new LibroDTO (isbn, titulo, numHojas, autor, fechaCreacion, paisDTO);
    }

    private LibroDTO getLibroFromResulset(ResultSet resultSet) throws SQLException {
        String isbn = resultSet.getString("isbn");
        String titulo = resultSet.getString("titulo");
        int numHojas = resultSet.getInt("num_hojas");
        String autor = resultSet.getString("autor");
        String codPais = resultSet.getString("cod_pais");
        PaisDTO paisDTO = csvPaisDAO.findByCod(codPais);
        LocalDateTime fechaCreacionTarea = resultSet.getTimestamp("fecha_creacion").toLocalDateTime();
        return new LibroDTO(isbn, titulo, numHojas, autor, fechaCreacionTarea, paisDTO);
    }
}
