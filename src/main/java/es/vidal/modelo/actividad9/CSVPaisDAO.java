package es.vidal.modelo.actividad9;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;

public class CSVPaisDAO {

    private final File FILE;

    public CSVPaisDAO(){

        FILE = new File("/home/batoi/IdeaProjects/PROG-UD12-CLASSWORK/src/main/resources/csvActividad9/paises.csv");

    }

    public ArrayList<PaisDTO> findAll() throws IOException {
        ArrayList<PaisDTO> paisArrayList = new ArrayList<>();
        try {
            Reader in = new FileReader(FILE);
            Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
            for (CSVRecord record : records) {
                String line = registerToString(record);
                PaisDTO productDTO = createProductDTO(line);
                paisArrayList.add(productDTO);
            }
        }catch (IOException exception){
            exception.getStackTrace();
        }
        return paisArrayList;
    }

    public String registerToString(CSVRecord record){
        return record.get(0) +
                "," +
                record.get(1) +
                "," +
                record.get(2) +
                "," +
                record.get(3) +
                "," +
                record.get(4) +
                "," +
                record.get(5);
    }

    public PaisDTO createProductDTO(String line){
        String[] productDTO = line.replace("\"", "").split(",");
        String nombre = productDTO[0].trim();
        String name = productDTO[1].trim();
        String nom = productDTO[2].trim();
        String iso2 = productDTO[3].trim();
        String iso3 = productDTO[4].trim();
        int phoneCode = 0;
        if (productDTO.length > 5){
            String phoneCodeString = productDTO[5].replace(" ", "");
             phoneCode = Integer.parseInt(phoneCodeString);
        }
        return new PaisDTO( nombre, name, nom, iso2, iso3, phoneCode);
    }

    public PaisDTO findByCod(String cod){
        try {
            Reader in = new FileReader(FILE);
            Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
            for (CSVRecord record : records) {
                String line = registerToString(record);
                PaisDTO productDTO = createProductDTO(line);
                if (productDTO.getIso2().equals(cod)){
                    return productDTO;
                }
            }
        }catch (IOException exception){
            exception.getStackTrace();
        }
        return null;
    }
}
