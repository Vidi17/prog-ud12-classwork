<%@ page import="es.vidal.modelo.actividad9.LibroDTO" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: batoi
  Date: 23/5/21
  Time: 22:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="header.jsp"/>
<%ArrayList<LibroDTO> libroDTOArrayList = (ArrayList<LibroDTO>) request.getAttribute("libros");%>
<body>
<table class="default" border="1">
    <tr>
        <th>ISBN</th>
        <th>Titulo</th>
        <th>NumHojas</th>
        <th>Autor</th>
        <th>Fecha Creación</th>
        <th>Código País</th>
    </tr>
    <% for (LibroDTO libro : libroDTOArrayList) { %>
    <tr>
        <td><%= libro.getIsbn()%></td>
        <td><%= libro.getTitulo()%></td>
        <td><%= libro.getNumHojas()%></td>
        <td><%= libro.getAutor()%></td>
        <td><%= libro.getFechaCreacion()%></td>
        <td><%= libro.getCodPais().getIso2()%></td>
    </tr>
    <% } %>
</table>
<a href="anyadir-libros">Crear libro</a>
</body>
<jsp:include page="footer.jsp"/>
</html>
