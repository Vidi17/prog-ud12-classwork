<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="es.vidal.modelo.actividad9.PaisDTO" %><%--
  Created by IntelliJ IDEA.
  User: batoi
  Date: 17/5/21
  Time: 17:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="header.jsp"/>
<body>
<h1>Crear Libro</h1>
<% HashMap<String, String> errors = null;
    if (request.getAttribute("añadido") != null) {%>
        <div>El libro ha sido añadido con éxito</div>
        <br>
    <% } else {
        errors = (HashMap<String, String>) request.getSession().getAttribute("errors");
    }
    String isbn = request.getParameter("isbn");
    String titulo = request.getParameter("titulo");
    int numHojas = 0;
    if (request.getParameter("numHojas") != null){
        numHojas = Integer.parseInt(request.getParameter("numHojas"));
    }
    String autor = request.getParameter("autor");
    LocalDateTime fechaCreacion = null;
    if (request.getParameter("fechaCreacion") != null){
    fechaCreacion = LocalDateTime.parse(request.getParameter("fechaCreacion"));
    }

    ArrayList<PaisDTO> paises = (ArrayList<PaisDTO>) request.getSession().getAttribute("paises");
    if (errors != null){
        if (errors.get("isbn") != null){ %>
            <div><%= errors.get("isbn")%></div>
        <%}
        if (errors.get("titulo") != null){ %>
            <div><%= errors.get("titulo")%></div>
        <%}%>
        <% if (errors.get("numHojas") != null){ %>
            <div><%= errors.get("numHojas")%></div>
        <%}%>
        <% if (errors.get("autor") != null){ %>
            <div><%= errors.get("autor")%></div>
        <%}%>
        <% if (errors.get("fechaCreacion") != null){ %>
            <div><%= errors.get("fechaCreacion")%></div>
        <%}%>
        <% if (errors.get("codPais") != null){ %>
            <div><%= errors.get("codPais")%></div>
        <%}%>
    <%}%>
<br>
<form id="libroForm" method="post" action="anyadir-libros">
    <label>
        <%if (isbn != null) {%>
        Introduzca el ISBN [##-##########-#] <input name="isbn" value="<%=isbn%>">
        <%} else {%>
        Introduzca el ISBN [##-##########-#] <input name="isbn">
        <%}%>
    </label>
    <br>
    <label>
        <%if (titulo != null) {%>
        Introduzca el Título (cadena entre 20 y 120)<input type="text" name="titulo" value="<%=titulo%>">
        <%} else {%>
        Introduzca el Título (cadena entre 20 y 120)<input type="text" name="titulo">
        <%}%>
    </label>
    <br>
    <label>
        <%if (numHojas != 0) {%>
        Introduzca el Número de hojas (número > 10)<input type="number" name="numHojas" value="<%=numHojas%>">
        <%} else {%>
        Introduzca el Número de hojas (número > 10)<input type="number" name="numHojas">
        <%}%>
    </label>
    <br>
    <label>
        <%if (autor != null) {%>
        Introduzca el Autor (cadena entre 10 y 50)<input type="text" name="autor" value="<%=autor%>">
        <%} else {%>
        Introduzca el Autor (cadena entre 10 y 50)<input type="text" name="autor">
        <%}%>
    </label>
    <br>
    <label>
        <% if (fechaCreacion != null){ %>
        Introduzca la fecha de Creación [Fecha y Hora dd-mm-yyyy hh:mm:ss]
        <input type="datetime-local" name="fechaCreacion" value="<%=fechaCreacion%>">
        <%} else {%>
        Introduzca la fecha de Creación [Fecha y Hora dd-mm-yyyy hh:mm:ss]
        <input type="datetime-local" name="fechaCreacion">
        <%}%>
    </label>
    <br>
    <label for="codPais"></label>
    Selecciona el País:
    <select name="codPais" id="codPais">
        <% for (PaisDTO pais: paises) {%>
        <option value="<%=pais.getIso2()%>"><%=pais.getNombre()%></option>
        <%}%>
    </select>
    <br>
    <label>
        <input type="submit" name="action" value="Crear">
    </label>
</form>
</body>
<jsp:include page="footer.jsp"/>
</html>
