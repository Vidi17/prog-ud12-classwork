<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Servidor de Aplicaciones</title>
</head>
<body>
<h1><%= "Servidor de Aplicaciones" %>
</h1>
<ul>
    <li><a href="bola-bingo">Generar Bola bingo</a></li>
    <li><a href="ver-hora">Ver hora</a></li>
    <li><a href="tabla-multiplicar">Tablas multiplicar</a></li>
    <li><a href="paso-parametros">Paso parametros</a></li>
    <li><a href="tabla-multiplicar-numero">Tablas multiplicar</a></li>
    <li><a href="buscar-usuarios">Buscar usuarios</a></li>
    <li><a href="listado-usuarios">Listado Usuarios</a></li>
    <li><a href="mostrar-tareas">Gestor Tareas TODO LIST</a></li>
    <li><a href="anyadir-libros">Añadir Libros</a></li>
</ul>
</body>
</html>