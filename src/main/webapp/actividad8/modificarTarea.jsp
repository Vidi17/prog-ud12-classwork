<%--
  Created by IntelliJ IDEA.
  User: batoi
  Date: 11/5/21
  Time: 23:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Modificar Tarea</title>
</head>
<body>
<h1>Modificar Tarea</h1>
<% String id = request.getParameter("id");%>
<% String fechaCreacion = request.getParameter("fechaCreacion");%>
<form id="tareaForm" method="post" action="modificar-tarea">
    <label>
        <textarea name="descripcion" rows="10" cols="20">Introduzca Descripción</textarea>
    </label>
    <br>
    <label>
        <input type="date" name="fechaEntrega">
    </label>
    <br>
    <label for="prioridad"></label>
    <select name="prioridad" id="prioridad">
        <option value="ALTA">ALTA</option>
        <option value="MEDIA">MEDIA</option>
        <option value="BAJA">BAJA</option>
    </select>
    <label>
        <input type="checkbox" name="realizada" value="isRealizada"> Realizada
    </label>
    <br>
    <label>
        <input type="hidden" name="fechaCreacion" value="<%=fechaCreacion%>">
        <input type="hidden" name="id" value="<%=id%>">
        <input type="submit" name="action" value="Modificar">
    </label>
</form>
</body>
</html>
