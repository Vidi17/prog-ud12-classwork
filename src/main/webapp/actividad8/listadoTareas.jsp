<%@ page import="es.vidal.modelo.actividad8.TareaDTO" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.time.Duration" %>
<%--
  Created by IntelliJ IDEA.
  User: batoi
  Date: 11/5/21
  Time: 23:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Listado Tareas</title>
</head>
<body>
<div>
    <h1>TODO LIST app</h1>
    <form action="buscar-tarea" method="get">
        <label>
            <input type="search" name="descripcion">
        </label>
        <label>
            <input type="checkbox" name="realizada" value="isRealizada"> Realizada
        </label>
        <input type="submit" name="action" value="Buscar">
    </form>

    <%ArrayList<TareaDTO> tareaDTOArrayList = (ArrayList<TareaDTO>) request.getAttribute("tareas");%>
    <table class="default" border="1">
        <tr>
            <th>Prioridad</th>
            <th>Descripción</th>
            <th>Vencimiento</th>
            <th>Realizada</th>
            <th>Acciones</th>
        </tr>
        <% for (TareaDTO tarea : tareaDTOArrayList) { %>
        <tr>
            <td><%= tarea.getPrioridad().toString()%></td>
            <td><%= tarea.getDescripcion()%></td>
            <td><%= Duration.between(tarea.getFechaCreacion(), tarea.getFechaEntrega()).toDays()%> dias</td>
            <%if (tarea.isRealizada()) { %>
            <td>
                <label>
                    <input type="checkbox" readonly checked name="realizada">
                </label>
            </td>
            <%} else { %>
            <td>
                <label>
                    <input type="checkbox" readonly name="realizada">
                </label>
            </td>
            <%}%>
            <%if (tarea.isRealizada()) {%>
            <td><form action="marcar-tarea" method="post">
                <input type="hidden" name="id" value="<%=tarea.getId()%>">
                <input type="submit" name="action" value="No realizada">
            </form>
            <%} else {%>
            <td><form action="marcar-tarea" method="post">
                <input type="hidden" name="id" value="<%=tarea.getId()%>">
                <input type="submit" name="action" value="Realizada">
            </form>
            <%}%>
            <td><form action="borrar-tarea" method="post">
                <input type="hidden" name="id" value="<%=tarea.getId()%>">
                <input type="submit" name="action" value="Borrar">
            </form>
            </td>
            <td><form action="modificar-tarea" method="get">
                <input type="hidden" name="id" value="<%=tarea.getId()%>">
                <input type="hidden" name="fechaCreacion" value="<%=tarea.getFechaCreacion()%>">
                <input type="submit" name="action" value="Modificar">
            </form>
            </td>
        </tr>
        <% } %>
    </table>
    <form action="añadir-tarea" method="post">
        <label>
            <textarea name="descripcion" rows="10" cols="20">Introduzca Descripción</textarea>
        </label>
        <label>
            <input type="date" name="fechaEntrega">
        </label>
        <label for="prioridad"></label>
        <select name="prioridad" id="prioridad">
            <option value="ALTA">Alta</option>
            <option value="MEDIA">Media</option>
            <option value="BAJA">Baja</option>
        </select>
        <label>
            <input type="submit" name="action" value="Añadir">
        </label>
    </form>
</div>
</body>
</html>
