<%@ page import="es.vidal.modelo.userDAO.UserDTO" %>
<%@ page import="es.vidal.modelo.userDAO.FileUserDAO" %><%--
  Created by IntelliJ IDEA.
  User: batoi
  Date: 4/5/21
  Time: 15:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Crear Usuario</title>
</head>
<body>
<a href="index.jsp">Volver</a>
<% String email = request.getParameter("email");
    if (email != null) {
        FileUserDAO fileUserDAO = new FileUserDAO();
        UserDTO userDTO = fileUserDAO.findByEmail(email);%>
    <h1>Ver Usuario</h1>
    <form id="userForm" method="post" action="listado-usuarios">
    <label> Nombre <input type="text" readonly name="name" value="<%=userDTO.getUsername()%>"/></label>
    <br>
    <label> Contraseña <input type="password" readonly name="password" value="<%=userDTO.getPassword()%>"/></label>
    <br>
    <label> Verificar Contraseña <input type="password" readonly name="repeatPassword" value="
<%=userDTO.getPassword()%>"/></label>
    <br>
    <label> Email <input type="email" readonly name="email" value="<%=userDTO.getEmail()%>"/></label>
    <br>
    <label> Fecha nacimiento <input type="date" readonly name="birthday" value="<%=userDTO.getBirthday()%>"/></label>
</form>
<% } else { %>
<h1>Crear Usuario</h1>
<form id="userForm" method="post" action="crear-usuario">
    <label> Nombre <input type="text" name="name"/></label>
    <br>
    <label> Contraseña <input type="password" name="password"/></label>
    <br>
    <label> Verificar Contraseña <input type="password" name="repeatPassword"/></label>
    <br>
    <label> Email <input type="email" name="email"/></label>
    <br>
    <label> Fecha nacimiento <input type="date" name="birthday"/></label>
    <br>
    <input type="submit" name = "action" value="Crear">
</form>
<% } %>
</body>
</html>
