<%@ page import="es.vidal.modelo.userDAO.UserDTO" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.time.Period" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="es.vidal.modelo.userDAO.UserDTO" %><%--
  Created by IntelliJ IDEA.
  User: batoi
  Date: 6/5/21
  Time: 19:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Listado Usuarios</title>
</head>
<body>
<div>
  <h1>Listado de Usuarios</h1>
  <%ArrayList<UserDTO> usuarios = (ArrayList<UserDTO>) request.getAttribute("usuarios");%>
  <a href="crearUsuario.jsp">Añadir nuevo usuario</a>
  <table class="default" border="1">
  <tr>
    <th>Username</th>
    <th>Email</th>
    <th>Edad</th>
    <th>Acciones</th>
  </tr>
    <% for (UserDTO usuario: usuarios) { %>
  <tr>
    <td><%= usuario.getUsername()%></td>
    <td><%= usuario.getEmail()%></td>
    <td><%= Period.between(usuario.getBirthday(), LocalDate.now()).getYears()%></td>
    <td><form action="listado-usuarios" method="post">
      <input type="hidden" name="email" value="<%=usuario.getEmail()%>">
      <input type="submit" name="action" value="Borrar">
    </form>
    <td><form action="crear-usuario" method="get">
      <input type="hidden" name="email" value="<%=usuario.getEmail()%>">
      <input type="submit" name="action" value="Ver detalle">
    </form>
  </td>
  </tr>
    <% } %>
  </table>
  Número total de registros : <%=usuarios.size()%>
</div>
</body>
</html>
