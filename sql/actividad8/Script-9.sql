CREATE DATABASE TODOLIST;

USE TODOLIST;

CREATE TABLE IF NOT EXISTS Tareas(
    id INT NOT NULL AUTO_INCREMENT,
    prioridad ENUM('ALTA','MEDIA','BAJA'),
    descripcion VARCHAR(50),
    fechaCreacion TIMESTAMP,
    fechaEntrega TIMESTAMP,
    realizada bit NOT NULL,
    PRIMARY KEY(id)
    );