CREATE DATABASE gestor_libros_db;

USE gestor_libros_db;

DROP TABLE IF EXISTS LIBROS;

CREATE TABLE IF NOT EXISTS LIBROS(
	isbn VARCHAR(15) NOT NULL,
	titulo VARCHAR(120),
	num_hojas INT,
	autor VARCHAR(50),
	fecha_creacion DATETIME,
	cod_pais VARCHAR(10),
	
	PRIMARY KEY(isbn)
	);